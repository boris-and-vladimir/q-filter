# Q-FILTER 2.0 #

![osi_standard__logo.png](https://bytebucket.org/lvsitanvs/q-filter/raw/ecd9eeb5e67abed2aab7b20054023df99b6a1025/img/osi_standard_logo.png?token=739f0ef4b7fbd86d715bd37cf89294cab4d568a2)

Q-Filter is an open source (GNU GPL licence - http://www.gnu.org/licences/gpl.html) app
that filters the data retrieved from the [Q Electronics](http://www.qelectronics.co.uk/) 
software for car GPS beacons, a txt file. It also helps to build an Excel file ready to 
the [Excel to the Kmz Transformer](https://bitbucket.org/lvsitanvs/excel-to-kmz-transformer) 
app, that reads an Excel file with geographic coordinates and draw points, polygons, 
linestrings, etc., in Google Earth.

### Table of Contents ###

* Project Structure
* Links to Manual and Examples
* Screenshots
* About

### Project Structure ###
* [build](https://bitbucket.org/lvsitanvs/q-filter/src/ecd9eeb5e67abed2aab7b20054023df99b6a1025/build/?at=master) - files to make the executable with auto-py-to-exe (https://pypi.org/project/auto-py-to-exe/)
* [distro](https://bitbucket.org/lvsitanvs/q-filter/src/ecd9eeb5e67abed2aab7b20054023df99b6a1025/distro/?at=master) - win64 exe file made auto-py-to-exe(https://pypi.org/project/auto-py-to-exe/)
* [docs](https://bitbucket.org/lvsitanvs/q-filter/src/ecd9eeb5e67abed2aab7b20054023df99b6a1025/docs/?at=master) - source code documentation
* [img](https://bitbucket.org/lvsitanvs/q-filter/src/ecd9eeb5e67abed2aab7b20054023df99b6a1025/img/?at=master) - project images
* [src](https://bitbucket.org/lvsitanvs/q-filter/src/ecd9eeb5e67abed2aab7b20054023df99b6a1025/src/?at=master) - project source code


### Requirements ###
* openpyxl (https://pypi.python.org/pypi/openpyxl)
* xlrd (https://pypi.python.org/pypi/xlrd)
* csv-to-sqlite (https://pypi.org/project/csv-to-sqlite/)

### Licence ###

![lgplv3-147x51.png](https://bytebucket.org/lvsitanvs/q-filter/raw/ecd9eeb5e67abed2aab7b20054023df99b6a1025/img/lgplv3-147x51.png?token=c0d5fd96f0a1df66f4611cec81cf67213c0c25f4)
Q-Filter 2.0

Filters data retrieved from [Q Electronics](http://www.qelectronics.co.uk/)
software for car GPS beacons. Helps to build an Excel file ready to
[Excel to the Kmz Transformer](https://bitbucket.org/lvsitanvs/excel-to-kmz-transformer)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
[GNU General Public License](http://www.gnu.org/licenses/) 
for more details.

### Screenshot ###

# Main Window
![Q-Filter.png](https://bytebucket.org/lvsitanvs/q-filter/raw/ecd9eeb5e67abed2aab7b20054023df99b6a1025/img/Q-Filter.png?token=b7d3ccf526aa971629d3e8a8f759c1fc0e270e61)

### About ###

* Developed at Boris & Vladimir Software, a fictitious team at DP - UAF - GNR, by Vladimir Oicnanev (Nuno Venâncio)