from sys import stdout
from codecs import getwriter
from os import path, sep
from business_logic import LogManipulator
from presentation import Window


if __name__ == '__main__':
    stdout = getwriter('utf8')(stdout)
    dir_path = path.dirname(path.realpath(__file__))
    log = LogManipulator(dir_path + sep + "log.log")
    log.write('Inicio do programa')
    Window(log)
