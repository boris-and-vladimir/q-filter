# #############################################################################
#                 PRESENTATION (VIEW) PACKAGE CLASSES
# #############################################################################
import sys
from codecs import getwriter
from tkinter import Tk, filedialog, Label, Button, Frame, Checkbutton, Canvas,\
    Menu, Toplevel, Radiobutton, IntVar, StringVar, W, E, NW, EW, FALSE
from sys import platform, exc_info, stdout
from business_logic import TxtManipulator, XlsManipulator, CsvManipulator, LogManipulator

if platform == 'linux':
    from os import O_RDONLY, open as o_open, sep, path
else:
    from os import startfile, sep, path


class Window(object):
    """
    Designs the Tk window interface
    """

    # ========================================================================
    #                             Constructor
    # ========================================================================
    def __init__(self, log):
        """
        FileIO -> Tk object

        Class constructor
        Designs the Tk window interface
        """
        # constants -----------------------------------------------------------
        self.master = Tk()
        self.master.title('Q - Filter 2.3')
        if platform != 'linux':
            self.master.iconbitmap(self.__set_icon())
            self.master.geometry("440x180")
        else:
            self.master.geometry("440x140")
        self.master.resizable(width=FALSE, height=FALSE)
        self.master.configure(borderwidth=1)
        self.log = log
        m1 = "Selecione filtro(s) antes de abrir um txt "
        m2 = "ou deixe em branco se abrir um xlsx ou csv"
        self.msg = m1 + m2
        self.bg = 'gray25'
        self.fg = 'white'
        self.ft = ('courier', 12, 'bold')

        # Variables -----------------------------------------------------------
        self.nans_var = IntVar(self.master)
        self.coords_var = IntVar(self.master)
        self.blanks_var = IntVar(self.master)
        self.repeated_var = IntVar(self.master)
        self.exec_file_var = IntVar(self.master)
        self.xls_tab_var = IntVar(self.master)
        self.xls_col_var = IntVar(self.master)
        self.is_read = False  # flag to know if the file was read
        self.file = None
        self.text_id = None  # the canvas message id
        self.selection = [0, 0, 0, 0]  # for the checkbuttons variables
        self.chosen_tab = StringVar(self.master)  # chosen xls tab
        self.chosen_col = StringVar(self.master)  # chosen excel date col
        self.userprefs = self.__get_user_prefs()  # prefs checkbuttons
        self.is_tab_chosen = False  # for the chosen excel tab
        self.is_col_chosen = False  # for the chosen excel date col

        # Draw Frames ---------------------------------------------------------
        self.frame1 = self.__draw_frame(self.master, 0, 2)
        self.frame2 = self.__draw_frame(self.master, 3, 3)

        # Draw Labels ---------------------------------------------------------
        self.__draw_label(self.frame2, "Log:", self.fg, 0, 1, stk=NW)
        self.__draw_label(self.frame1, "Filtros txt:", self.fg, 0, 2)
        self.__draw_label(self.frame2, "Boris & Vladimir Software",
                          "limegreen", 4, 3, font=("Quartz Ms", 9))

        # Draw Checkbuttons ---------------------------------------------------
        self.cb_NaNs = self.__draw_checkbutton(self.frame1, 'Nans',
                                               self.nans_var,
                                               self.__bind_chkbtn_NaNs, 2)
        self.cb_coords = self.__draw_checkbutton(self.frame1,
                                                 'Coordenadas próximas  ',
                                                 self.coords_var,
                                                 self.__bind_chkbtn_coords, 1)
        self.cb_blanks = self.__draw_checkbutton(self.frame1,
                                                 'Linhas em branco',
                                                 self.blanks_var,
                                                 self.__bind_chkbtn_blanks, 3)
        self.cb_repeated = self.__draw_checkbutton(self.frame1,
                                                   'Linhas repetidas',
                                                   self.repeated_var,
                                                   self.__bind_chkbtn_reps, 4)

        # Draw Buttons --------------------------------------------------------
        self.__draw_button(self.master, 'Abrir...', self.__open_file, 0, W)
        self.__draw_button(self.master, 'Gravar', self.__save_file, 1, W)
        self.__draw_button(self.master, 'Sair', self.__quit, 5, E)

        # Draw Canvas to display the program messages -------------------------
        self.canvas = self.__draw_canvas()

        # Put the first message to canvas -------------------------------------
        self.__set_message(self.msg)

        # Draw Menu and SubMenus ----------------------------------------------
        self.menu = Menu(self.master)
        self.master.config(menu=self.menu)
        self.__draw_sub_menu('Ficheiro', ['Abrir...', 'Gravar', 'Sair'],
                             [self.__open_file, self.__save_file, self.__quit])
        pref_labels = ['Abrir ficheiro após filtragem',
                       'Escolher manualmente o separador Excel',
                       'Escolher manualmente a coluna de data de referência',
                       'Guardar Preferências']
        pref_commands = [self.__bind_chkbtn_exec, self.__bind_chkbtn_xls_sep,
                         self.__bind_chkbtn_xls_col, self.__set_user_prefs]
        self.__draw_sub_menu('Preferências', pref_labels, pref_commands,
                             n_checkbuttons=3)
        self.__draw_sub_menu('Ajuda', ['Sobre', 'Ver log', 'Manual'],
                             [self.__show_about,
                              self.__show_log,
                              self.__show_man])

        # Window Mainloop -----------------------------------------------------
        self.master.mainloop()

    # ========================================================================
    #                            Private methods
    # ========================================================================
    # --------------------------- draw methods -------------------------------
    def __set_message(self, message):
        """
        str -> None

        message - the message to display on canvas

        Draws a message in the window canvas
        """
        if self.text_id:
            self.canvas.delete(self.text_id)
        if platform == 'linux':
            self.text_id = self.canvas.create_text(3, 3, anchor=NW,
                                                   text=message, width=255,
                                                   fill=self.fg)
        else:
            self.text_id = self.canvas.create_text(3, 3, anchor=NW,
                                                   text=message, width=267,
                                                   fill=self.fg)

    def __draw_frame(self, window, column, columnspan):
        """
        Tk, int, int -> Frame

        window - the Tk window in witch the Frame is draw
        column - the grid column in witch the Frame is draw
        columnspan - the number of columns in witch the frame is spanned

        Draws a Frame in the window
        """
        frame = Frame(window, borderwidth=2, bg=self.bg)
        frame.grid(row=0, column=column, rowspan=5, columnspan=columnspan,
                   padx=1, pady=1)

        return frame

    def __draw_label(self, f, text, fg, row, colspan, stk=None, font=None):
        """
        Frame, str, str, int, NW, tuple -> None

        f - a Frame object to draw the Label
        text - the text for the Label
        fg - the foreground color of the Label
        row - the grid row in witch the Label is designed
        colspan - the number of columns in witch the Label is spanned
        stk - the positional anchor to display the text of the Label
        font - the font type for the text of the Label

        Draws a Label in a Frame
        """
        if font:
            label = Label(f, text=text, font=font, bg=self.bg, fg=fg)
        else:
            label = Label(f, text=text, font=self.ft, bg=self.bg, fg=fg)

        if stk:
            label.grid(row=row, column=0, sticky=stk)
        else:
            label.grid(row=row, column=0, columnspan=colspan)

    def __draw_checkbutton(self, f, text, variable, command, row):
        """
        Frame str, IntVar, function, int -> Checkbutton

        f - the frame in witch the Checkbutton will be draw
        text - the text of the Checkbutton
        variable - the IntVar associated with the Checkbutton
        command - the method to execute when the Checkbutton is selected
        row - the grid row in witch the Checkbutton is designed

        Draws a Checkbutton in a Frame
        """
        checkbutton = Checkbutton(f, text=text, bg=self.bg,
                                  fg=self.fg, selectcolor='black',
                                  variable=variable, command=command)
        checkbutton.grid(row=row, column=0, columnspan=2, sticky=W)

        return checkbutton

    def __draw_radiobutton(self, f, text, variable):
        """
        Frame, str -> None

        f - the Frame where the radiobutton will be draw
        text - the text and value of the radiobutton
        variable - variable to bind the radiobuttons

        Draws a Radiobutton in a Frame
        """
        radiobutton = Radiobutton(f, text=text, variable=variable, value=text,
                                  indicatoron=0)
        radiobutton.grid(column=0, columnspan=2, sticky=EW)

    def __draw_button(self, window, text, command, column, sticky):
        """
        Tk window, str, function, int, W or E -> None

        window - the Tk Window in witch the button will be draw
        text - the text of the Button
        command - the method to execute when the Button is pressed
        column - the grid column in witch the Button is designed
        sticky - the positional anchor to display the Button on the Tk Window

        Draws a Button on the Tk Window
        """
        if platform == 'linux':
            button = Button(window, width=7, text=text, command=command)
        else:
            button = Button(window, width=10, text=text, command=command)
        button.grid(row=5, column=column, sticky=sticky, pady=2)

    def __draw_canvas(self):
        """
        None -> Canvas

        Draws a Canvas in a Frame
        """
        if platform == 'linux':
            canvas = Canvas(self.frame2, width=255, height=60)
        else:
            canvas = Canvas(self.frame2, width=267, height=75)
        canvas.config(bg='black')
        canvas.grid(row=1, rowspan=3, column=0, columnspan=3)

        return canvas

    def __draw_sub_menu(self, label, labels, commands, n_checkbuttons=0):
        """
        str, list, list, int -> None

        label - name to show in Menu
        labels - names to show in commands and / or checkbuttons
        commands - the commands to bind
        n_checkbuttons - number of checkbuttons to draw

        Draws the submenus in Menu
        """
        menu = Menu(self.menu)
        self.menu.add_cascade(label=label, menu=menu)

        if n_checkbuttons:
            c_labels = [labels[i] for i in range(n_checkbuttons)]
            labels = labels[n_checkbuttons:]
            c_commands = [commands[i] for i in range(n_checkbuttons)]
            commands = commands[n_checkbuttons:]
            variables = [self.exec_file_var, self.xls_tab_var,
                         self.xls_col_var]

            for i in range(len(c_labels)):
                menu.add_checkbutton(label=c_labels[i],
                                     command=c_commands[i],
                                     variable=variables[i])
        for i in range(len(labels)):
            menu.add_command(label=labels[i], command=commands[i])

    def __draw_toplevel(self, title):
        """
        str -> Toplevel

        title - the title of the Toplevel Window

        Draws a Toplevel Tk Window
        """
        toplevel = Toplevel(self.master)
        toplevel.configure(borderwidth=5)
        toplevel.title(title)
        if platform != 'linux':
            toplevel.iconbitmap(self.__set_icon())
        toplevel.resizable(width=FALSE, height=FALSE)

        return toplevel

    # ---------------------- event biding methods -----------------------------
    def __bind_chkbtn_coords(self):
        """
        None -> None

        Binds the IntVar associated with the Checkbutton to the selection list
        property
        """
        if self.selection[0] == 0:
            self.selection[0] = 1
            self.log.write('Filtro por coordenadas selecionado')
        else:
            self.selection[0] = 0
            self.log.write('Filtro por coordenadas selecionado')

    def __bind_chkbtn_NaNs(self):
        """
        None -> None

        Binds the IntVar associated with the Checkbutton to the selection list
        property
        """
        if self.selection[1] == 0:
            self.selection[1] = 1
            self.log.write('Filtro por NaNs selecionado')
        else:
            self.selection[1] = 0
            self.log.write('Filtro por NaNs desselecionado')

    def __bind_chkbtn_blanks(self):
        """
        None -> None

        Binds the IntVar associated with the Checkbutton to the selection list
        property
        """
        if self.selection[2] == 0:
            self.selection[2] = 1
            self.log.write('Filtro por linhas em branco selecionado')
        else:
            self.selection[2] = 0
            self.log.write('Filtro por linhas em branco desselecionado')

    def __bind_chkbtn_reps(self):
        """
        None -> None

        Binds the IntVar associated with the Checkbutton to the selection list
        property
        """
        if self.selection[3] == 0:
            self.selection[3] = 1
            self.log.write('Filtro por linhas repetidas selecionado')
        else:
            self.selection[3] = 0
            self.log.write('Filtro por linhas repetidas desselecionado')

    def __bind_chkbtn_exec(self):
        """
        None -> None

        Binds the IntVar associated with the Checkbutton (execute file)
        """
        if self.userprefs[0] == 0:
            self.userprefs[0] = 1
            self.exec_file_var.set(value=1)
            self.log.write('Abrir ficheiro após filtragem selecionado')
        else:
            self.userprefs[0] = 0
            self.exec_file_var.set(value=0)
            self.log.write('Abrir ficheiro após filtragem desselecionado')

    def __bind_chkbtn_xls_sep(self):
        """
        None -> None

        Binds the IntVar associated with the Checkbutton (show excel tabs) to
        the __show_excel_tabs method
        """
        if self.userprefs[1] == 0:
            self.userprefs[1] = 1
            self.xls_tab_var.set(value=1)
            self.log.write('Escolher separador Excel selecionado')
        else:
            self.userprefs[1] = 0
            self.xls_tab_var.set(value=0)
            self.log.write('Escolher separador Excel desselecionado')

    def __bind_chkbtn_xls_col(self):
        """
        None -> none

        Binds the IntVar associated with the Checkbutton (show excel headers)
        to the __show_excel_columns method
        """
        if self.userprefs[2] == 0:
            self.userprefs[2] = 1
            self.xls_col_var.set(value=1)
            self.log.write('Escolher coluna de Excel selecionado')
        else:
            self.userprefs[2] = 0
            self.xls_col_var.set(value=0)
            self.log.write('Escolher coluna de Excel desselecionado')

    def __open_file(self):
        """
        None -> None

        Binds the 'Open File...' Button to a filedialog, then calls the
        accordingly file manipulator
        """
        msg = 'Ficheiro a filtrar \ limpar...'
        self.log.write('Clique no botão "Abrir"')
        filepath = filedialog.askopenfilename(title=msg)
        filename = filepath[filepath.rfind(sep) + 1:]
        self.log.write('ficheiro ' + filename + ' escolhido')

        if filepath != () and filepath != '':
            extension = path.splitext(filepath)[1]

            # ------------------------ txt part -------------------------------
            if extension == '.txt':
                try:
                    self.file = TxtManipulator(filepath, self.log)
                    self.log.write(filename + ' carregado com sucesso')
                    self.__set_message('Ficheiro carregado com sucesso')

                    self.is_read = self.file.txt_to_line_list("txt")
                    self.log.write(filename + ' - Filtros selecionados')
                    self.__set_message('A aplicar filtro(s)...')

                    self.file.clean_txt(self.selection, "txt")
                    self.log.write(filename + ' - filtrado com sucesso')
                    m = 'Ficheiro filtrado \ limpo com sucesso\nClique Gravar'
                    self.__set_message(m)

                except IOError:
                    e = exc_info()[1]
                    self.__set_message('Erro ao ler ficheiro!\n %s' % str(e))
                    self.log.write('Window.__open_file\n' + str(e))
                    self.log.close()

            # ------------------------ csv part -------------------------------
            if extension == '.csv':
                try:
                    self.file = CsvManipulator(filepath, self.log)
                    self.log.write(filename + ' carregado com sucesso')
                    self.__set_message('Ficheiro carregado com sucesso')

                    self.is_read = self.file.csv_to_line_list(True, ";")
                    self.log.write(filename + ' - Carregada com sucesso para a memória')
                    self.__set_message('A aplicar filtro(s)...')

                    self.file.clean_csv("_temp.csv")
                    self.log.write(filename + ' - filtrado com sucesso')
                    m = 'Ficheiro filtrado \ limpo com sucesso\nClique Gravar'
                    self.__set_message(m)

                except IOError:
                    e = exc_info()[1]
                    self.__set_message('Erro ao ler ficheiro!\n %s' % str(e))
                    self.log.write('Window.__open_file\n' + str(e))
                    self.log.close()
            
            # ------------------------ xls part -------------------------------
            elif extension == '.xlsx':
                try:
                    self.file = XlsManipulator(filepath, self.log)
                    worksheet_names = self.file.read()

                    if self.xls_tab_var.get():
                        self.is_tab_chosen = False
                        self.__show_excel_tabs(worksheet_names, filename)

                    elif self.xls_col_var.get():
                        headers = self.file.choose_worksheet()
                        self.is_col_chosen = False
                        self.__show_excel_columns(headers, filename)
                    else:
                        self.file.choose_reference_col()
                        self.__xls_to_db(filename)
                        self.is_read = True

                except IOError:
                    e = exc_info()[1]
                    self.__set_message('Erro ao ler ficheiro!\n %s' % str(e))
                    self.log.write('Window.__open_file\n' + str(e))
                    self.log.close()

            # ------------------- other extensions part -----------------------
            else:
                m = 'O ficheiro escolhido não tem um formato válido.\n' + \
                    'Escolha um txt, um csv ou um xlsx'
                self.log.write(filename + ' Escolha errada de ficheiro')
                self.__set_message(m)

    def __save_file(self):
        """
        None -> None

        Binds the 'Save' Button to save the manipulated file
        """
        if self.is_read:
            try:
                # -------------- txt part -------------------------------------
                if type(self.file) is TxtManipulator:
                    file = self.file.write_txt()
                    self.log.write(file + ' - gravado com sucesso')
                    self.__reset_window()
                    if self.exec_file_var.get():
                        try:
                            if platform == 'linux':
                                o_open(file, O_RDONLY)
                            else:
                                startfile(file)
                        except EnvironmentError:
                            if platform == 'linux':
                                o_open(self.__get_dir_path() + sep + file,
                                       O_RDONLY)
                            else:
                                startfile(self.__get_dir_path() + sep + file)

                # ------------------ csv part ---------------------------------
                elif type(self.file) is CsvManipulator:
                    file = self.file.write_csv()
                    self.log.write(file + ' gravado com sucesso')
                    self.__reset_window()
                    if self.exec_file_var.get():
                        try:
                            if platform == 'linux':
                                o_open(file, O_RDONLY)
                            else:
                                startfile(file)
                        except EnvironmentError:
                            if platform == 'linux':
                                o_open(self.__get_dir_path() + sep + file,
                                       O_RDONLY)
                            else:
                                startfile(self.__get_dir_path() + sep + file)

                # -------------- xls part -------------------------------------
                elif type(self.file) is XlsManipulator:
                    if self.chosen_col.get() == 'Data':
                        file = self.file.write()
                    else:
                        file = self.file.write(is_date_col=False)
                    self.log.write(file + ' gravado com sucesso')
                    self.__reset_window()
                    if self.exec_file_var.get():
                        try:
                            if platform == 'linux':
                                o_open(file, O_RDONLY)
                            else:
                                startfile(file)
                        except EnvironmentError:
                            if platform == 'linux':
                                o_open(self.__get_dir_path() + sep + file,
                                       O_RDONLY)
                            else:
                                startfile(self.__get_dir_path() + sep + file)

                # ------------ others part ------------------------------------
                else:
                    m = 'Antes de gravar tem de abrir um ficheiro txt, csv ou xlsx\n'
                    self.log.write('Clique no botão "Gravar" sem antes abrir')
                    self.__set_message(m + self.msg)
            except WindowsError:
                e = exc_info()[1]
                self.__set_message('Erro ao gravar ficheiro!\n %s' % str(e))
                self.log.write('Window.__save_file\n' + str(e))
                self.log.close()

    def __quit(self):
        """
        None -> None

        Binds the 'Quit' Button to destroy the Tk Window
        """
        self.master.destroy()
        self.log.close()

    def __show_about(self):
        """
        None -> None

        Associated with the Help Menu.
        Creates a new window with the "About" information
        """
        appversion = "2.3 "
        app = " Q-Filter"
        copyright = '(c) 2014' + 2 * ' ' + \
            'SDATO - DP - UAF - GNR'
        txt0 = '''
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
               '''
        lic = 'https://www.gnu.org/licenses/gpl-3.0.txt'
        contactname = "Nuno Venâncio"
        contactphone = "(00351) 969 564 906"
        contactemail = "venancio.gnr@gmail.com\n"
        txt1 = "Version: " + appversion + 2 * "\n"
        spc = 9 * ' '
        txt2 = spc + "Copyleft: " + copyright + "\n" + spc + "Licença: " + lic
        txt3 = contactname + '\n' + contactphone + '\n' + contactemail
        fg = 'white'
        font1 = ('courier', 20, 'bold')
        font2 = ('courier', 9)

        toplevel = self.__draw_toplevel('Sobre...')
        frame = self.__draw_frame(toplevel, 0, 2)
        self.__draw_label(frame, app, fg, 0, 2, stk=W, font=font1)
        self.__draw_label(frame, txt1, fg, 1, 1, stk=E)
        self.__draw_label(frame, txt2, fg, 2, 1, stk=W, font=font2)
        self.__draw_label(frame, txt0, fg, 3, 1, stk=W, font=font2)
        self.__draw_label(frame, txt3, fg, 4, 3, font=font2)
        self.__draw_button(toplevel, 'Ok', toplevel.destroy, 1, W)

    def __show_excel_tabs(self, tabs, filename):
        """
        list, str -> None

        tabs - a list containing the tab names
        filename - the complete filename path

        Associated with the Preferences Menu
        Creates a new window with checkbuttons corresponding to the excel
        tabs, so the user can select the tab in witch the program should work
        """
        fg = 'white'
        toplevel = self.__draw_toplevel('Separadores')
        frame = self.__draw_frame(toplevel, 0, 2)
        self.__draw_label(frame, "Selecione o separador Excel", fg, 0, 2,
                          font=self.ft)

        def destroy():
            if self.chosen_tab.get():
                self.is_tab_chosen = True
                message = 'Tab ' + self.chosen_tab.get() + ' selecionada'
                self.log.write(message)
                self.__set_message(message)

                worksheet_name = self.chosen_tab.get()
                headers = self.file.choose_worksheet(worksheet_name)
                self.chosen_tab.set('')

                if self.xls_col_var.get():
                    toplevel.destroy()
                    self.__show_excel_columns(headers, filename)
                else:
                    toplevel.destroy()
                    self.file.choose_reference_col()
                    self.__xls_to_db(filename)

            else:
                self.log.write('Nenhuma tab excel escolhida')
                self.__set_message('Escolha uma tab Excel')

        for i in range(len(tabs)):
            self.__draw_radiobutton(frame, tabs[i], self.chosen_tab)

        self.__draw_button(toplevel, 'Ok', destroy, 1, W)

    def __show_excel_columns(self, cols, filename):
        """
        list, str -> None

        cols - a list containing the excel headers
        filename - the complete filename path

        Associated with the Preferences Menu
        Creates a new window with checkbuttons corresponding to the excel
        columns, so the user can select the one in witch the program
        should work
        """
        fg = 'white'
        toplevel = self.__draw_toplevel('Colunas')
        frame = self.__draw_frame(toplevel, 0, 2)
        self.__draw_label(frame, "Selecione a coluna de referência",
                          fg, 0, 2, font=self.ft)

        def destroy():
            if self.chosen_col.get():
                self.is_col_chosen = True
                message = 'Coluna ' + self.chosen_col.get() + ' escolhida'
                self.log.write(message)
                self.__set_message(message)
                index = cols.index(self.chosen_col.get())
                self.chosen_col.set('')
                self.file.choose_reference_col(index)
                toplevel.destroy()
                self.__xls_to_db(filename)

        for i in range(len(cols)):
            self.__draw_radiobutton(frame, cols[i], self.chosen_col)

        self.__draw_button(toplevel, 'Ok', destroy, 1, W)

    def __show_log(self):
        """
        None -> None

        Opens the log file
        """
        if platform == 'linux':
            o_open(self.__get_dir_path() + sep + 'log.log', O_RDONLY)
        else:
            startfile(self.__get_dir_path() + sep + 'log.log')

    def __show_man(self):
        """
        None -> None

        Opens the manual file
        """
        if platform == 'linux':
            o_open(self.__get_dir_path() + sep + 'manual.pdf', O_RDONLY)
        else:
            startfile(self.__get_dir_path() + sep + 'manual.pdf')

    # -------------------------- other helper methods -------------------------
    def __get_user_prefs(self):
        """
        None - list

        Returns a list of boolean ints corresponding to the user preferences
        """
        filename = self.__get_dir_path() + sep + 'userprefs'
        try:
            userprefs = TxtManipulator(filename, self.log)
            line_list = [0, 0, 0]
            if userprefs.txt_to_line_list("txt") or userprefs.txt_to_line_list("csv"):
                line_l = userprefs.get_line_list()
                line_list = [int(line[0][0]) for line in line_l]

            self.exec_file_var.set(value=line_list[0])
            self.xls_tab_var.set(value=line_list[1])
            self.xls_col_var.set(value=line_list[2])
            self.log.write('Preferências carregadas com sucesso')

            return line_list

        except IOError:
            e = exc_info()[1]
            self.__set_message('Erro ao ler ficheiro de preferêcias!\n %s' %
                               str(e))
            self.log.write('Window.__get_user_prefs\n' + str(e))

    def __set_user_prefs(self):
        """
        None -> None

        Saves the user preferences to the userprefs file
        """
        filename = self.__get_dir_path() + sep + 'userprefs'

        try:
            userprefs = TxtManipulator(filename, self.log)
            line_list = []

            for i in self.userprefs:
                line_list.append(str(i) + '\n')
            userprefs.set_line_list(line_list)
            userprefs.write_txt(set_new_filename=False)
            self.__set_message('Preferências gravadas com sucesso')
            self.log.write('Preferências alteradas com sucesso')
            self.userprefs = self.__get_user_prefs()
        except IOError:
            e = exc_info()[1]
            self.__set_message('Erro ao gravar ficheiro de preferêcias!\n %s' %
                               str(e))
            self.log.write('Window.__set_user_prefs\n' + str(e))

    def __xls_to_db(self, filename):
        """
        str -> None

        filename - the complete filename path

        Turns the excel values in db values
        """
        self.file.xls_to_db()
        self.is_read = True
        m = 'Ficheiro filtrado com sucesso\nClique Gravar'
        self.log.write(filename + ' filtrado com sucesso')
        self.__set_message(m)

    def __reset_window(self):
        """
        None -> None

        Resets the Tk Window, file and selection fields, IntVars associated
        with the Checkbuttons, and sets a new message
        """

        self.is_read = False
        self.cb_NaNs.deselect()
        self.cb_coords.deselect()
        self.cb_blanks.deselect()
        self.cb_repeated.deselect()
        self.file = None
        self.selection = [0, 0, 0, 0]
        self.__set_message('Ficheiro gravado com sucesso\n' + self.msg)

    def __set_icon(self):
        """
        None -> str

        Returns the path of the icon file
        """
        return self.__get_dir_path() + sep + "Q.ico"

    def __get_dir_path(self):
        """
        None -> str

        Returns the path of the working directory
        """
        return path.dirname(path.realpath(__file__))


if __name__ == '__main__':
    stdout = getwriter('utf8')(stdout)
    dir_path = path.dirname(path.realpath(__file__))
    log = LogManipulator(dir_path + sep + "log.log")
    sys.stderr = log
    log.write('Inicio do programa')
    Window(log)
